//
//  ForeCastTableViewCell.swift
//  WeatherApplication
//
//  Created by Vasanth on 8/7/2019.
//  Copyright © 2019 Vasanth. All rights reserved.
//

import UIKit

class ForeCastTableViewCell: UITableViewCell {
    @IBOutlet weak var cellImgView: UIImageView!
    @IBOutlet weak var readingsLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var humidityLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadView(data : List){

            let value = data.main?.temp ?? 0.0
            let celVal  = Int((value - 273.15).rounded())
            readingsLbl.text = String(celVal) + "°C"
            descLbl.text = data.weather?.first?.main ?? ""
            dateLbl.text = Date.timeFormat(date: data.dtTxt!)
            humidityLbl.text = String(data.main?.humidity ?? 0)
        
        if let icon = data.weather?.first?.icon{
             cellImgView.imgIcon(icon)
        }else{
            cellImgView.imgIcon("01n")
        }
       
        
    }

    
}
