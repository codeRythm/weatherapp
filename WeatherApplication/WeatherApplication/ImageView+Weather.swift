//
//  ImageView+Weather.swift
//  WeatherApplication
//
//  Created by Vasanth on 8/7/2019.
//  Copyright © 2019 Vasanth. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    public func imgIcon(_ icon: String) {
        let urlString = String(format: "http://openweathermap.org/img/wn/%@@2x.png", icon)
        if let url = URL(string: urlString) {
            DispatchQueue.global().async {
                if let dataImg = try? Data(contentsOf: url), let image = UIImage(data: dataImg) {
                    DispatchQueue.main.async {
                        self.image = image
                    }
                }
            }
        }
    }
}
