//
//  ViewController.swift
//  WeatherApplication
//
//  Created by Vasanth on 8/7/19.
//  Copyright © 2019 Vasanth. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var temperaturLbl: UILabel!
    @IBOutlet weak var weatherDescLbl: UILabel!
    @IBOutlet weak var humidityLbl: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    var  groupedWeather :[String : [List]]!
    var  weatherSectionKeys :[String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()
        requestAPIWeather()
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        
        if let weekday = Date.getDayOfWeek(today: result){
            print(weekday)
        }
        
        var timer = Timer.scheduledTimer(timeInterval: 500.0, target: self, selector: #selector(ViewController.requestAPIWeather), userInfo: nil, repeats: true)
    }
    
    func registerCell(){
        let cell  = UINib(nibName: "ForeCastTableViewCell", bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: "ForeCastTableViewCell")
        self.tblView.dataSource = self
    }
    
    
    
    
    @objc func requestAPIWeather() {
        
        
        //        APIServiceLayer.request(router: Router.getCurrentWeather) { result in
        //            switch result {
        //            case .success:
        //                print(result)
        //            case .failure:
        //                print(result)
        //            }
        //        }
        APIServiceLayer.request(router: Router.getWeather) {result in
            switch result {
            case .success(let data):
                self.currentWeather(data: data)
                print(data)
            case .failure:
                print(result)
            }
        }
        
    }
    
    func groupedEpisodesByMonth(_ weatherList: [List]) -> [String: [List]] {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let sortedlist = weatherList.sorted(by: { dateFormatter.date(from:$0.dtTxt!)!.compare(dateFormatter.date(from:$1.dtTxt!)!) == .orderedAscending })
        
        print(sortedlist)
        
        var empty = [String: [List]] ()
        //        return weatherList.reduce(into: empty) { acc, cur in
        //            var calendar = Calendar.current
        //            calendar.timeZone = TimeZone(identifier: "UTC")!
        //            let dateConv = dateFormatter.date(from: cur.dtTxt!)!
        //            let components = calendar.dateComponents([.day], from: dateConv)
        
        var acc : [String: [List]] = [:]
        for cur in weatherList {
            
            var dayTxt = ""
            
            let calender = Calendar.current
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let date = dateFormatter.date(from:  cur.dtTxt!){
                
                if calender.isDateInToday(date) {
                    dayTxt = "Today"
                }else if calender.isDateInTomorrow(date){
                    dayTxt = "Tomorrow"
                }else {
                    dateFormatter.dateFormat = "E, d MMM"
                    dayTxt = dateFormatter.string(from: date)
                }
            }
            
            
            let existing = acc[dayTxt] ?? []
            acc[dayTxt] = existing + [cur]
            
            print(acc)
            
            
            // let date = Calendar.current.date(from: components)!
            //  let existing = acc[dayTxt] ?? []
            // let existing = acc[dayTxt] ?? []
            // acc[dayTxt] = existing + [cur]
            
            // acc[dayTxt] = cur
        }
        
        print(acc)
        return acc
        // return acc
    }
    
    func currentWeather (data : Data){
        
        let weatherData = try? JSONDecoder().decode(Weather.self, from: data)
        
        
        groupedWeather = groupedEpisodesByMonth((weatherData?.list)!)
        weatherSectionKeys = Array(groupedWeather.keys)
        
        tblView.reloadData()
        
        //var arr: [List] = Array(groupedWeather["Today"])
        
        if let arr = groupedWeather["Today"]{
            let current = arr.last
            DispatchQueue.main.async {
                
                self.humidityLbl.text =  "Humidity: " + String(current?.main?.humidity ?? 0) + "%"
                
                let value = current?.main?.temp ?? 0.0
                let celVal  = Int((value - 273.15).rounded())
                
                self.temperaturLbl.text = String(celVal) + "°C"
                self.weatherDescLbl.text =  current?.weather?.first?.weatherDescription ?? "-"
            }
            
            if let icon = current?.weather?.first?.icon{
                imgView.imgIcon(icon)
            }else{
                imgView.imgIcon("01n")
            }
        }
        
    }
}

extension ViewController: UITableViewDataSource,UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return weatherSectionKeys?.count ?? 0
        
        //return weatherSectionKeys.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return weatherSectionKeys?[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let key:String = (weatherSectionKeys?[section])!
        let arr  = Array(groupedWeather[key]!)
        
        return arr.count
        
        //return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell  = tableView.dequeueReusableCell(withIdentifier: "ForeCastTableViewCell") as? ForeCastTableViewCell
        {
            let key:String = (weatherSectionKeys?[indexPath.section])!
            let arr  = Array(groupedWeather[key]!)
            
            cell.loadView(data: arr[indexPath.row])
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    
}
