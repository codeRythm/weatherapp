//
//  ApiClient.swift
//  WeatherApplication
//
//  Created by Vasanth on 8/7/19.
//  Copyright © 2019 Vasanth. All rights reserved.
//

import Foundation
//
//// TODO: - Move to the separated file Resource.swift
//struct Resource {
//    let url: URL
//    let method: String = "GET"
//}
//
//// TODO: - Move to the separated file GenericResult.swift
//enum Result<T> {
//    case success(T)
//    case failure(Error)
//}
//
//enum APIClientError: Error {
//    case noData
//}
//
//// TODO: - Move to the separated file URLRequest+Resource.swift
//extension URLRequest {
//
//    init(_ resource: Resource) {
//        self.init(url: resource.url)
//        self.httpMethod = resource.method
//    }
//
//}
//
//final class APIClient {
//
//    func load(_ resource: Resource, result: @escaping ((Result<Data>) -> Void)) {
//        let request = URLRequest(resource)
//        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
//            guard let `data` = data else {
//                result(.failure(APIClientError.noData))
//                return
//            }
//            if let `error` = error {
//                result(.failure(error))
//                return
//            }
//            result(.success(data))
//        }
//        task.resume()
//    }
//
//}
