//
//  Weather.swift
//  WeatherApplication
//
//  Created by Vasanth on 8/7/19.
//  Copyright © 2019 Vasanth. All rights reserved.
//

import Foundation

// MARK: - Weather
struct Weather: Codable {
    var cod: String?
    var message: Double?
    var cnt: Int?
    var list: [List]?
    var city: City?
}


struct City: Codable {
    let id: Int?
    let name: String?
    let coord: Coord?
    let country: String?
    let population, timezone: Int?
}


struct Coord: Codable {
    let lat, lon: Double?
}


struct List: Codable {
    //let dt: String?
    var main: MainClass?
    var weather: [WeatherElement]?
    var clouds: Clouds?
//    var wind: Wind?
//   //let rain: Rain?
//    var sys: Sys?
    var dtTxt: String?
    
    enum CodingKeys: String, CodingKey {
        case main, weather, clouds
        case dtTxt = "dt_txt"
    }
}


struct Clouds: Codable {
    let all: Int?
}


struct MainClass: Codable {
    let temp, tempMin, tempMax, pressure: Double?
    let seaLevel, grndLevel: Double?
    let humidity: Int?
    let tempKf: Double?
    
    enum CodingKeys: String, CodingKey {
        case temp
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
        case humidity
        case tempKf = "temp_kf"
    }
}

struct Rain: Codable {
    //let the3H: String?
    
//    enum CodingKeys: String, CodingKey {
//        case the3H = "3h"
//    }
}

struct Sys: Codable {
    let pod: Pod?
}

enum Pod: String, Codable {
    case d = "d"
    case n = "n"
}

struct WeatherElement: Codable {
    let id: Int?
    let main: String?
    let weatherDescription: String?
    let icon: String?
    
    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription = "description"
        case icon
    }
}

enum MainEnum: String, Codable {
    case clear = "Clear"
    case clouds = "Clouds"
    case rain = "Rain"
}

enum Description: String, Codable {
    case brokenClouds = "broken clouds"
    case clearSky = "clear sky"
    case fewClouds = "few clouds"
    case lightRain = "light rain"
    case overcastClouds = "overcast clouds"
    case scatteredClouds = "scattered clouds"
    case moderateRain = "moderate rain"
}


struct Wind: Codable {
    let speed, deg: Double?
}

