//
//  Router.swift
//  WeatherApplication
//
//  Created by Vasanth on 8/7/19.
//  Copyright © 2019 Vasanth. All rights reserved.
//

import Foundation
//"http://api.openweathermap.org/data/2.5/forecast?q=Singapore&APPID=400db7622e647612468e9fe1c4b5aaf8")

//http://api.openweathermap.org/data/2.5/weather?q=Singapore&appid=400db7622e647612468e9fe1c4b5aaf8

enum Router {
    case getWeather
    case getCurrentWeather
    
    var scheme: String {
        switch self {
        case .getWeather, .getCurrentWeather:
            return "http"
        }
    }
    
    var host: String {
        switch self {
        case .getWeather, .getCurrentWeather:
            return "api.openweathermap.org"
        }
    }

    var path: String {
        switch self {
        case .getWeather:
            return "/data/2.5/forecast"
        case .getCurrentWeather:
            return "/data/2.5/weather"
        }
    }
    
    var parameters: [URLQueryItem] {
        let accessToken = "400db7622e647612468e9fe1c4b5aaf8"
        switch self {
        case .getWeather,.getCurrentWeather:
            return [URLQueryItem(name: "q", value: "Singapore"),
                    URLQueryItem(name: "APPID", value: accessToken)]
            
        }
    }
    
    var method: String {
        switch self {
        case .getWeather,.getCurrentWeather:
            return "GET"
        }
    }

}


class APIServiceLayer {
    
    class func request(router: Router, completion: @escaping (Result<Data, Error>) -> Void) {
        
        var components = URLComponents()
        components.scheme = router.scheme
        components.host = router.host
        components.path = router.path
        components.queryItems = router.parameters
        
        guard let url = components.url else { return }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = router.method
        
        let session = URLSession(configuration: .default)
        
        let dataTask = session.dataTask(with: urlRequest) { data, response, error in
            guard error == nil else {
                completion(.failure(error!))
                print(error?.localizedDescription as Any)
                return
            }
            
            guard response != nil else {
                return
            }
            
            guard let data = data else {
                return
            }
    
            DispatchQueue.main.async {
                completion(.success(data))
                
            }
            
        }
        
        dataTask.resume()
        
    }
    
}



//            let responseObject = try! JSONDecoder().decode([String: [T]].self, from: data)

//            let jsonResponse = try JSONSerialization.jsonObject(with:

//                dataResponse, options: [])

//            debugPrint(jsonResponse)



//            let response = try JSONDecoder().decode(Weather.self, from: data)
            
