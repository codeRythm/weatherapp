//
//  Date+Weather.swift
//  WeatherApplication
//
//  Created by Vasanth on 8/7/19.
//  Copyright © 2019 Vasanth. All rights reserved.
//

import Foundation

extension Date{
    static func getDayOfWeek(today: String) -> Int? {
        
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let todayDate = formatter.date(from: today) {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let myComponents = myCalendar.components(.weekday, from: todayDate)
            let weekDay = myComponents.weekday
            return weekDay
        } else {
            return nil
        }
    }
    
    
    static func timeFormat( date :String) -> String {
    
        var time : String? = nil
        
        let calender = Calendar.current
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = dateFormatter.date(from: date){
            
            dateFormatter.dateFormat = "h:mm a"
            time = dateFormatter.string(from: date)
        }
        return time ?? ""
    }
}
